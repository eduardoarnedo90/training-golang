package main

import "fmt"

func main() {
	x := 42 + 1
	y := "Eduard Arnedo"
	fmt.Println(x)
	fmt.Println(y)
	x = 29
	fmt.Println(x)
}
