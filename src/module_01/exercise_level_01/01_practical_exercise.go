package main

//Usando el operador de declaración corta, ASIGNA los siguientes VALORES a VARIABLES con los IDENTIFICADORES “x”, “y” y “z”
//   * - 42
//   * - “James Bond”
//   * - true
//Luego imprime los valores almacenados en esas variables usando
//   * - Una sola declaración de la función println
//   * - Múltiples declaraciones de println

import "fmt"

func main() {
	x := 42
	y := "James Bond"
	z := true

	fmt.Println(x, y, z)

	fmt.Println(x)
	fmt.Println(y)
	fmt.Println(z)
}
