package main

//En scope a nivel de paquete, asigna los siguientes valores a las tres variables
// 	* - a x asignale 42
//	* - a y asignale “James Bond”
//	* - a z asignale true
//en main
//	* - Usa fmt.Sprintf para imprimir todos los VALORES en un solo string. ASIGNA el valor retornado de TIPO string usando el
//		operador de declaración corta a  la VARIABLE con el IDENTIFICADOR “s”
// * - Imprime el valor almacenado por la variable “s”

import (
	"fmt"
)

var x int = 42
var y string = "James Bons"
var z bool = true

func main() {
	s := fmt.Sprintf("%v\t%v\t%v", x, y, z)
	fmt.Println(s)
}
