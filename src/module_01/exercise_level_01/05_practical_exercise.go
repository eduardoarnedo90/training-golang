package main

//Ahora haces esto
//	* - Ahora usa CONVERSIÓN para convertir el TIPO del VALOR almacenado en “x” al TIPO IMPLÍCITO
//	* - Usa el operador “=” para ASIGNAR ese valor a “y”
//	* - Imprime el valor almacenado en “y”
//	* -	Imprime el tipo de “y”

import (
	"fmt"
)

type numero int

var x numero
var y int

func main() {

	fmt.Println(x)
	fmt.Printf("El tipo de x es: %T\n", x)

	x = 42
	fmt.Println(x)

	y = int(x)
	fmt.Println(y)
	fmt.Printf("El tipo de y es: %T\n", y)

}
