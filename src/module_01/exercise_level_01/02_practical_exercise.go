package main

//Usa var para DECLARAR tres VARIABLES. Las variables deben tener scope a nivel de paquete. No asignar VALORES a las variables.
//Usa los siguientes IDENTIFICADORES para las variablesy asegúrate de que las variables son de los siguientes TIPOS (lo quiere decir que pueden
//almacenar VALORES de ese TIPO).
//	* - identificador “x” tipo int
//	* - identificador “y” tipo string
//	* - identificador “z” tipo bool
//En main
//	* - Imprime los valores de cada identificador
//	* - El compilador asigna valores a las variables. ¿Cómo son llamados esos valores?

import (
	"fmt"
)

var x int
var y string
var z bool

func main() {
	fmt.Println(x)
	fmt.Println(y)
	fmt.Println(z)
}
