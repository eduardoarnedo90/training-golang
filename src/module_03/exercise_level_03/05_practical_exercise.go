package main

import "fmt"

//Imprime el resto o módulo, el cual es resultado de dividir entre 4 cada número en el rango de 10 y 100.

func main() {
	for number := 10; number < 100; number++ {
		fmt.Printf("Cuando dividimos %v entre 4, el resto es %v\n", number, number%4)
	}
}
