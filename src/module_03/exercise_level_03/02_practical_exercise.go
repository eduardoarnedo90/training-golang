package main

import "fmt"

//Imprime el rune code point de las letras del alfabeto en mayúsculas tres veces.

func main() {
	for i := 65; i <= 90; i++ {

		fmt.Println(i)

		for x := 0; x < 3; x++ {
			fmt.Printf("\t%#U\n", i)
		}

	}
}
