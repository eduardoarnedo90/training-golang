package main

import "fmt"

//Crea un programa que use una declaración switch con la expresión de switch especificada como una variable de
//TIPO string y el IDENTIFICADOR “deporteFav”.

func main() {
	deporteFav := "béisbol"
	switch deporteFav {
	case "béisbol":
		fmt.Println("Ve al estadio")
	case "natación":
		fmt.Println("Ve a la piscina")
	case "crossfit":
		fmt.Println("Te quiero ver en los crossfit games.")
	}
}
