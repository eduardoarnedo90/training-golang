package main

import "fmt"

//Usando el ejercicio anterior, crea un programa que use “else if” y “else”.

func main() {
	x := "Robin"
	if x == "Robin" {
		fmt.Println(x)
	} else if x == "Batman" {
		fmt.Println("Batman", x)
	} else {
		fmt.Println("Ningún súper héroe")
	}
}
