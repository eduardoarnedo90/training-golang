package main

import "fmt"

//Crea un programa que use una declaración switch sin expresión especificada.

func main() {
	switch {
	case false:
		fmt.Println("No debería imprimir")
	case true:
		fmt.Println("Debería imprimir")
	}
}
