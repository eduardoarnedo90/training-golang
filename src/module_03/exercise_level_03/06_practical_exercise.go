package main

import "fmt"

//Crea un programa que muestre el “if statement” en acción.

func main() {
	x := "Batman"
	if x == "Batman" {
		fmt.Println(x)
	}
}
