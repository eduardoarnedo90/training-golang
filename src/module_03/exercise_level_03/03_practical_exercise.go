package main

import "fmt"

//Crea un ciclo usando la siguiente sintaxis for condición
//	* - Haz que imprima los años que has vivido.

func main() {
	bird := 1991
	for bird <= 2020 {
		fmt.Println(bird)
		bird++
	}
}
