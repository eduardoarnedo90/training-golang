package main

import "fmt"

//Crea un ciclo for usando esta sintaxis for { }
//	* Haz que imprima los años que has vivido.

func main() {
	bird := 1991

	for {
		if bird <= 2020 {
			fmt.Println(bird)
		} else {
			break
		}
		bird++
	}
}
