package main

import "fmt"

func main() {
	x := 434
	if x == 40 {
		fmt.Println("El valor es 40")
	} else if x == 41 {
		fmt.Println("El valor es 41")
	} else {
		fmt.Println("El valor no es 40 ni 41")
	}
}
