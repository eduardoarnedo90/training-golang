package main

import "fmt"

//Crea una variable de tipo string usando un string literal no interpretado (raw string literal). Imprímelo.

func main() {
	a := `here is something
		as
		a
		raw string
		literal
		"you see"
		another thing`
	fmt.Println(a)
}
