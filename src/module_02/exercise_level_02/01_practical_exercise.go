package main

import "fmt"

//Escribe un programa que imprima un número en decimal, binario, y hexadecimal

func main() {

	x := 42
	fmt.Printf("%d\t%b\t%x\n", x, x, x)
	fmt.Printf("%d\t%b\t%#x", x, x, x)
}
