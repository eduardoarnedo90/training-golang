package main

import "fmt"

//Crea constantes CON TIPO y SIN TIPO. Imprime el valor de las mismas.

const a = 10
const b int = 20

func main() {
	fmt.Println(a, b)
	fmt.Printf("%T\t%T", a, b)
}
