package main

import "fmt"

// Usando iota, crea 4 constantes para los PRÓXIMOS 4 años. Imprime los valores de las constantes.

const (
	a = 2017 + iota
	b = 2017 + iota
	c = 2017 + iota
	d = 2017 + iota
)

func main() {

	fmt.Println(a)
	fmt.Println(b)
	fmt.Println(c)
	fmt.Println(d)
}
