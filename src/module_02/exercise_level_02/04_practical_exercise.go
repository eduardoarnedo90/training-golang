package main

import "fmt"

//Escribe un programa que
//	* -  Asignar un int a una variable
//	* -  Imprímelo en decimal, binario, y hex
//	* -  Has shifts de bits de ese int una posición a la izquierda y asigna eso a una variable
//	* - Imprime esa variable en decimal, binario, y hex

func main() {
	a := 42
	fmt.Printf("%d\t%b\t%#x\n", a, a, a)

	b := a << 1
	fmt.Printf("%d\t%b\t%#x", b, b, b)
}
