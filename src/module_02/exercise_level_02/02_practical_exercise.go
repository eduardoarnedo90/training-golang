package main

import "fmt"

//Usando los siguientes operadores, escribe expresiones y asigna sus valores a variables:

var x int
var y string
var z bool

func main() {
	a := (42 == 42)
	b := (42 <= 43)
	c := (42 >= 45)
	d := (42 != 43)
	e := (42 < 46)
	f := (42 > 43)

	fmt.Println(a, b, c, d, e, f)
}
